# Exploring [jspm](http://jspm.io/)

## Try it
With `git`, `npm`, and `jspm`  and `serve` installed on the system

* Clone this repository
* `cd` into the directory
* Run: `jspm install && serve .`
* Visit: http://localhost:3000/
* View console log output in the browser